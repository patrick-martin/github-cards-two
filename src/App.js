import React from "react";
import "semantic-ui-css/semantic.min.css";
import { Card, Image, Icon } from "semantic-ui-react";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      isActive: false
    };
  }

  handleToggle = () => {
    //this.setState (can omit 'this' when calling handleToggle later on, as arrow function binds 'this' to always refer to this particular instance)
    if (this.state.isActive === true) {
      this.setState({ isActive: false });
    } else {
      fetch("https://api.github.com/users/thurt")
        .then(resp => resp.json())
        .then(resultObject => {
          //console.log(resultObject.login);
          this.setState({ user: resultObject, isActive: true });
        });
    }
  };

  render() {
    //this.state.user = {} initially; after fetch request, it will be filled with our JS user object
    return (
      <div>
        <button onClick={this.handleToggle}>Toggle User</button>
        {this.state.isActive && ( //if isActive = true, display this stuff --> first time App is rendered on page load, it is false.  Second time, setState sets it to true.
          <Card>
            <Image src={this.state.user.avatar_url} wrapped ui={false} />
            <Card.Content>
              <Card.Header>{this.state.user.name}</Card.Header>
              <Card.Meta>
                <span className="date">
                  Joined on: {this.state.user.created_at}
                </span>
              </Card.Meta>
              <Card.Description>
                Taylor is an amazing Facilitator at Kenzie Academy. This is his
                bio for some reason: {this.state.user.bio}
              </Card.Description>
            </Card.Content>
            <Card.Content extra>
              <Icon name="user" />
              Followers: {this.state.user.followers}
            </Card.Content>
          </Card>
        )}
      </div>
    );
  }
}

export default App;

// {
//   /* <React.Fragment>
//             <img src={this.state.user.avatar_url} />
//             <p>{this.state.user.name}</p>
//             <p>{this.state.user.bio}</p>
//             <p>{this.state.user.followers}</p>
//           </React.Fragment> */
// }
